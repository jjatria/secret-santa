use Test2::V0;

use Acme::SecretSanta::Participant;
use Acme::SecretSanta::Group;

like dies { Acme::SecretSanta::Participant->new }, qr/required/,
  'Constructor fails with no name';

is +Acme::SecretSanta::Participant->new( name => 'test' ), object {
  call exceptions => hash { field test => E; end };
  call [ can => $_ ] => T for qw(
    address
    exceptions
    groups
    language
    name
    target

    join_group
    leave_group
    add_exceptions
    remove_exceptions
    reset
  );
}, 'Participant automatically excludes self';

subtest target => sub {
  my $a = Acme::SecretSanta::Participant->new( name => 'bar' );
  my $b = Acme::SecretSanta::Participant->new( name => 'bar' );

  is +Acme::SecretSanta::Participant->new( name => 'foo' ), object {
    call [ target => $b ] => T;
    call target => exact_ref $b;
    call reset  => T;
    call target => U;
  };
};

subtest groups => sub {
  my $p = Acme::SecretSanta::Participant->new( name => 'foo' );

  is $p->total_groups, 0, 'Participant starts with no groups';

  my $a = Acme::SecretSanta::Group->new( name => 'a' );
  my $b = Acme::SecretSanta::Group->new( name => 'b' );

  $p->join_group( $a );
  is $p->total_groups, 1, 'Participant can join groups';

  $p->join_group( $a );
  is $p->total_groups, 1, 'Joining groups is idempotent';

  $p->leave_group( $a );
  is $p->total_groups, 0, 'Participant can leave groups';

  $p->leave_group( $a );
  is $p->total_groups, 0, 'Leaving groups is idempotent';

  $p->join_group( $a, $b );
  is $p->total_groups, 2, 'Participant can join multiple groups';

  $p->leave_group( $a, $b );
  is $p->total_groups, 0, 'Participant can leave multiple groups';
};

done_testing;
