use Test2::V0;

use Acme::SecretSanta::Dispatcher::Handle;
use Capture::Tiny 'capture';

is +Acme::SecretSanta::Dispatcher::Handle->new, object {
  call [ does => 'Acme::SecretSanta::Role::Dispatcher' ] => T;
  call [ can => $_ ] => T for qw( output dispatch );
}, 'Dispatcher does the role';

subtest dispatch => sub {
  my $self = Acme::SecretSanta::Dispatcher::Handle->new;

  is [ capture { $self->dispatch( 'Test' ) } ], array {
    item match qr/test/i;
    item '';
    etc;
  }, 'Handle dispatcher prints to STDOUT';

  $self->output( \*STDERR );

  is [ capture { $self->dispatch( 'Test' ) } ], array {
    item '';
    item match qr/test/i;
    etc;
  }, 'Handle dispatcher prints to STDERR';

  open my $fh, '>', \my $out;
  $self->output($fh);

  $self->dispatch( 'Test' );
  like $out, qr/test/i, 'Handle dispatcher prints to arbitrary handle';
};

done_testing;
