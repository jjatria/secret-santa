use Test2::V0;

use Acme::SecretSanta::Sorter::Shuffle;

is +Acme::SecretSanta::Sorter::Shuffle->new, object {
  call [ does => 'Acme::SecretSanta::Role::Sorter' ] => T;
  call [ can => $_ ] => T for qw( sort retry );
}, 'Renderer does the role';

done_testing;
