use Test2::V0;

use Acme::SecretSanta::Participant;
use Acme::SecretSanta::Renderer::Email;

is +Acme::SecretSanta::Renderer::Email->new, object {
  call [ does => 'Acme::SecretSanta::Role::Renderer' ] => T;
  call [ can => $_ ] => T for qw( render render_summary );
}, 'Renderer does the role';

subtest render => sub {
  my $self = Acme::SecretSanta::Renderer::Email->new;

  my $foo = Acme::SecretSanta::Participant->new(
    name     => 'foo',
    address  => 'foo@foosome.com',
    language => 'es',
  );

  my $bar = Acme::SecretSanta::Participant->new(
    name    => 'bar',
    address => 'bar@barton.com',
  );

  $foo->target($bar);
  $bar->target($foo);

  is $self->render($foo), object {
    prop blessed => 'Email::Stuffer';
    call as_string => match qr/te toc=C3=B3 bar/i;
  }, 'Renders template in Spanish';

  is $self->render($bar), object {
    prop blessed => 'Email::Stuffer';
    call as_string => match qr/you got foo/i;
  }, 'Renders template in English';
};

done_testing;
