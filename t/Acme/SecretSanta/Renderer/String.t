use Test2::V0;

use Acme::SecretSanta::Renderer::String;
use Acme::SecretSanta::Participant;

is +Acme::SecretSanta::Renderer::String->new, object {
  call [ does => 'Acme::SecretSanta::Role::Renderer' ] => T;
  call [ can => $_ ] => T for qw( render render_summary );
}, 'Renderer does the role';

subtest render => sub {
  my $self = Acme::SecretSanta::Renderer::String->new;

  my $foo = Acme::SecretSanta::Participant->new(
    name    => 'foo',
    address => 'Foosome',
  );
  my $bar = Acme::SecretSanta::Participant->new(
    name    => 'bar',
    address => 'Barton',
  );

  $foo->target($bar);

  like $self->render($foo), qr/foo \] --> \[ bar/i,
    'Renders as string';

  $self->format('%N <- %n');
  like $self->render($foo), qr/bar <- foo/i,
    'Can reformat string';

  $self->format('%a -> %A');
  like $self->render($foo), qr/Foosome -> Barton/i,
    'Format string can access address';

  $self->format('%n %s to %N');
  like $self->render($foo, 'gives'), qr/gives to/i,
    'Formatter also uses sprintf';

};

done_testing;
