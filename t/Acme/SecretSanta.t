use Test2::V0;

use Acme::SecretSanta;

use File::Share ':all';
use Path::Tiny qw( path );
use JSON::MaybeXS qw( decode_json );
use Acme::SecretSanta::Renderer::Email;
use Capture::Tiny ':all';

use Acme::SecretSanta::Participant;

is +Acme::SecretSanta->new, object {
  call [ can => $_ ] => T for qw(
    groups
    dispatcher
    sorter
    renderer
    participants
    is_sorted
    total_participants
    participants
    remove_participant
    add_participant
    dispatch
    render
    sort
    load
  );
  call is_sorted => F;
  call sorter => object { call needs_retry => F };
}, 'Renderer does the role';

subtest participants => sub {
  my $santa = Acme::SecretSanta->new;

  is $santa->total_participants, 0, 'Participants default to empty';

  $santa->add_participant(
    Acme::SecretSanta::Participant->new( name => 'foo' ) );

  is $santa->total_participants, 1, 'Added one participant';
  is $santa->get_participant('foo'), object {
    prop blessed => 'Acme::SecretSanta::Participant';
  };

  $santa->add_participant('bar');
  is $santa->total_participants, 2, 'Added one participant from string';
  is $santa->get_participant('bar'), object {
    prop blessed => 'Acme::SecretSanta::Participant';
  };

  $santa->remove_participant('foo');
  is $santa->get_participant('foo'), U, 'Removed participant';

  $santa->add_participant('foo', 'baz');
  is $santa->total_participants, 3, 'Added list of participants';

  $santa->remove_participant('bar', 'baz');
  is $santa->total_participants, 1, 'Removed list of participants';
};

subtest sorter => sub {
  my $santa = Acme::SecretSanta->new;
  my $sorter = $santa->sorter;

  $santa->add_participant('foo');

  like dies { $santa->sort }, qr/not enough participants/i,
    'sort fails with too few participants';

  $santa->add_participant('bar', 'baz');
  is $santa->sort, T,
    'sort succeeds with three or more participants';

  is $santa->is_sorted, T,
    'Santa is_sorted after sorting';

  like dies { $santa->sort }, qr/already sorted/i,
    'sort fails if already sorted';

  is $sorter->reset, T, 'reset succeeds';

  is $santa->is_sorted, F,
    'Santa is not sorted after resetting';

  my $tries = 0;
  $santa->sorter->once( assign => sub {
    is $_[0]->retry, T, 'retry succeeds';
    is $_[0]->needs_retry, T, 'Sorter needs retry after calling retry';
  });

  $santa->sorter->on( sort => sub { $tries++ } );

  $sorter->sort;

  is $tries, 1, 'Retried once';

  $santa->add_participant( 'zoo' );

  is $santa->is_sorted, F,
    'Santa is not sorted after adding participants';

  $sorter->sort;

  $santa->remove_participant( 'zoo' );

  is $santa->is_sorted, F,
    'Santa is not sorted after removing participants';

  my ( $foo, $bar, $baz ) = $santa->participants;

  like dies { $sorter->assign_target },
    qr/not enough arguments/i,
    'Cannot assign target without arguments';

  like dies { $sorter->assign_target($foo, undef) },
    qr/target is not defined/i,
    'Cannot assign target without defined target';

  like dies { $sorter->assign_target( undef, $bar ) },
    qr/source is not defined/i,
    'Cannot assign target without defined source';

  is $sorter->assign_target($foo, $bar), T, 'Assign target suceeds';
  is $foo->target, exact_ref $bar, 'Target properly assigned';

  like dies { $sorter->assign_target($foo, $baz) },
    qr/already has a target/i,
    'Cannot assign target if source already has target';

  $sorter->reset;

  is $foo->target, U, 'Resetting sorter clears assigned targets';

  $foo->add_exceptions( $baz );

  like dies { $sorter->assign_target($foo, $baz) },
    qr/in source exception list/i,
    'Cannot assign target if it is in exception list';

  $sorter->reset;

  $sorter->assign_target($foo, $bar);
  $sorter->assign_target($bar, $foo);
  is $sorter->needs_retry, F,
    'Can assign crossed participants';

  $sorter->reset;
  $sorter->on( assign => $sorter->no_crossings );

  $sorter->assign_target($foo, $bar);
  $sorter->assign_target($bar, $foo);
  is $sorter->needs_retry, T,
    'Can disable assignment of crossed participants';

  is scalar(@{$sorter->subscribers( 'assign' )}), 1,
    'Has subscribers';

  $sorter->reset;

  is scalar(@{$sorter->subscribers( 'assign' )}), 1,
    'Reset does not clear subscribers';

  $sorter->clear_subscribers;

  is scalar(@{$sorter->subscribers( 'assign' )}), 0,
    'Can clear subscribers';

  my ($zoo) = $santa->add_participant( 'zoo' );

  $santa->set_group( 'a' => $foo, $bar );
  $santa->set_group( 'b' => $baz, $zoo );

  $sorter->on( assign => $sorter->no_group_intersections );

  $sorter->assign_target($foo, $bar);
  $sorter->assign_target($zoo, $baz);

  is $sorter->needs_retry, F,
    'Can assign without group intersections';

  $sorter->reset;

  $sorter->assign_target($foo, $zoo);
  $sorter->assign_target($bar, $baz);

  is $sorter->needs_retry, T, 'Can detect group intersections';
};

subtest load => sub {
  my $file = path(dist_dir 'Acme-SecretSanta')->child('data/test.json');
  my $data = decode_json $file->slurp;

  like dies { Acme::SecretSanta->load },
    qr/not enough arguments/i,
    'Load dies when not given enough arguments';

  is +Acme::SecretSanta->load( $file->canonpath ), object {
     call total_participants => 6;
  }, 'Loader succeeds with filename';

  is +Acme::SecretSanta->load( $data ), object {
     call total_participants => 6;
  }, 'Loader succeeds with arrayref';

  # Remove exceptions, because otherwise
  # they'll make reference to non-existing participants
  delete $data->[0]{exceptions};

  is +Acme::SecretSanta->load( $data->[0] ), object {
    call total_participants => 1;
  }, 'Loader succeeds with single hashref';

  my $renderer = Acme::SecretSanta::Renderer::Email->new;
  my $santa    = Acme::SecretSanta->load( $data => { renderer => $renderer } );

  is $santa, object {
    call renderer => exact_ref $renderer;
    call renderer => object { call santa => exact_ref $santa };
  }, 'Loader succeeds with additional data';
};

subtest groups => sub {
  my $santa = Acme::SecretSanta->new;

  my ($foo, $bar, $baz) = $santa->add_participant(qw( foo bar baz ));

  is $santa->total_groups, 0, 'Santa starts with no groups';

  my $group = $santa->set_group( 'a' => $foo );
  is $santa->total_groups, 1, 'Groups are created from participant methods';

  is $group->is_member( $foo ), T, 'Foo is member of group';
  is $group->is_member( $bar ), F, 'Bar is not member of group';

  $foo->leave_group( $group );
  is $santa->total_groups, 1, 'There can be empty groups';

  $group->add_member( $foo, $bar );
  is $group->total_members, 2, 'Can get group members';

  $foo->leave_group( $group );

  my ($a) = $santa->get_group('a')->members;
  is $a, exact_ref $bar, 'Group members returns participants';
};

subtest pipeline => sub {

  my $file = path(dist_dir 'Acme-SecretSanta')->child('data/test.json');
  my $santa = Acme::SecretSanta->load( $file->canonpath );

  like dies { $santa->renderer->render_summary },
    qr/unsorted santa object/i, 'Cannot summarise if not sorted';

  $santa->sort;

  my $summary = $santa->renderer->render_summary;
  is [ split( /\n/, $summary ) ],
    array { prop size => $santa->total_participants; etc },
    'String summary has resulst for all participants';

  my @participants = $santa->participants;
  like my $msg = $santa->render( $participants[1] ),
    qr/\w+ \] --> \[ \w+/i, 'Can render through shortcut';

  is [ capture { $santa->dispatch( $msg ) } ], array {
    item match qr/\w+ \] --> \[ \w+/i;
    etc;
  }, 'Can dispatch through shortcut';
};

done_testing;
