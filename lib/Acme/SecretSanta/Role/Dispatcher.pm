package Acme::SecretSanta::Role::Dispatcher;

our $VERSION = '2022';

use Moo::Role;

with 'Acme::SecretSanta::Role::Base';

use namespace::clean;

requires 'dispatch';

'Last Christmas I gave you my heart';
