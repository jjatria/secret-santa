package Acme::SecretSanta::Role::Sorter;

our $VERSION = '2022';

use Moo::Role;
use List::Util 'any';
use Carp;

with 'Acme::SecretSanta::Role::Base';
with 'Role::EventEmitter';

use namespace::clean;

# The sort method should return self, to allow for chaining
requires 'sort';
before sort => sub { shift->emit('sort'); };

sub retry { shift->{retry} = 1 }
sub needs_retry { shift->{retry} }

before reset => sub {
  my $self = shift;

  delete $self->{retry};
  $self->santa->is_sorted(0);

  $self->{todo} = { map { $_->name => $_ } $self->santa->participants };

  $_->reset for $self->santa->participants;
};

sub unassigned_names { keys %{ shift->{todo} //= {} } }

sub clear_subscribers {
  my ($self) = shift;
  $self->unsubscribe($_) for qw( sort assign );
}

sub assign_target {
  my ($self, $source, $target) = @_;

  croak 'Not enough arguments to assign'
    unless scalar @_ == 3;

  croak 'Source is not defined'
    unless defined $source;

  croak 'Target is not defined'
    unless defined $target;

  croak 'Source already has a target'
    if defined $source->target;

  croak 'Target is in source exception list'
    if defined $source->exceptions->{$target->name};

  $source->target( delete $self->{todo}{$target->name} );
  $self->emit( assign => $source );

  1;
}

sub no_group_intersections {
  sub {
    my ($sorter, $source) = @_;

    for my $group ($source->groups) {

      for my $team_mate ($group->members) {
        next if $team_mate->name eq $source->name;

        if (defined $team_mate->target) {
          my @source_groups    = $source->target->groups;
          my @team_mate_groups = $team_mate->target->groups;

          return $sorter->retry if do {
            my %e; @e{@source_groups} = ();
            any { exists $e{$_} } @team_mate_groups;
          };
        }
      }
    }
  };
}

sub no_crossings {
  sub {
    my ($sorter, $source) = @_;
    if (defined $source->target->target) {
      return $sorter->retry if $source->target->target eq $source;
    }
  };
}

'I saw mommy kissing Santa Claus';
