package Acme::SecretSanta::Role::Participant;

our $VERSION = '2022';

use Moo::Role;

with 'Acme::SecretSanta::Role::Base';

use Types::Standard qw( Undef Str );
use Acme::SecretSanta::Types 'SantaParticipant';
use List::Util 'any';
use Scalar::Util 'weaken';
use Ref::Util 'is_blessed_ref';
use Carp;

use namespace::clean;

has name     => ( is => 'ro', isa => Str, required => 1 );
has language => ( is => 'ro', isa => Str, default => 'en' );
has address  => ( is => 'ro', isa => Str|Undef );

has target => (
    is      => 'rw',
    isa     => SantaParticipant|Undef,
    clearer => 1,
);

sub total_groups { scalar shift->groups }

sub groups { values %{ shift->{groups} //= {} } }

sub join_group {
  my $self = shift;

  croak 'Cannot add member: need a list of references'
    if any { !is_blessed_ref $_ } @_;

  weaken( $self->{groups}{ $_->name } = $_ ) for @_;

  $self;
}

sub leave_group {
  my $self = shift;

  for (@_) {
    $_->remove_member( $self );
    delete $self->{groups}{ $_->name };
  }

  $self;
}

sub add_exceptions {
  my $self = shift;

  weaken( $self->{exceptions}{ $_->name } = $_ ) for @_;

  $self;
}

sub remove_exceptions {
  my $self = shift;
  delete @{ $self->{exceptions} //= {} }{ map $_->name, @_ };
  $self;
};

sub list_exceptions { keys %{ shift->{exceptions} //= {} } }

sub exceptions { shift->{exceptions} //= {} }

before reset => sub { shift->clear_target };

sub BUILD {
  my ($self) = @_;
  $self->add_exceptions($self);
}

'Let it snow, let it snow, let it snow';
