package Acme::SecretSanta::Role::Base;

our $VERSION = '2022';

use Moo::Role;

use namespace::clean;

has santa => ( is => 'rw', weak_ref => 1 );

sub reset { shift }

'Let it snow, let it snow, let it snow';
