package Acme::SecretSanta::Dispatcher::Handle;

our $VERSION = '2022';

use Moo;

with 'Acme::SecretSanta::Role::Dispatcher';

use namespace::clean;

has output => ( is => 'rw', lazy => 1, default => sub { \*STDOUT } );

sub dispatch { shift->output->print(@_) }

q{And how does Frosty handle / All of the snow that's slowly melting away};
