package Acme::SecretSanta::Dispatcher::Email;

our $VERSION = '2022';

use Moo;

with 'Acme::SecretSanta::Role::Dispatcher';

use Email::Sender::Simple;
use Email::Sender::Transport::SMTP;
use Feature::Compat::Try;
use Time::HiRes 'sleep';

use feature 'isa';

has [qw( port server username password )] => ( is => 'ro' );

has ssl     => ( is => 'ro', default => 1 );
has timeout => ( is => 'ro', default => 5 );
has retries => ( is => 'ro', default => 5 );

sub dispatch {
  my ($self, $mail) = @_;

  $mail = $mail->email;

  print 'Sending email notification to ', $mail->header('to'), "\n";

  $self->{transport} //= Email::Sender::Transport::SMTP->new(
    host          => $self->server,
    port          => $self->port,
    timeout       => $self->timeout,
    ssl           => $self->ssl,
    sasl_username => $self->username,
    sasl_password => $self->password,
  );

  for my $attempt ( 0 .. $self->retries ) {
    if ($attempt) {
      print "Retrying email send. This is attempt number $attempt\n";
      my $backoff = ( 2 ** $attempt ) * ( 1 + rand 0.2 );
      sleep $backoff;
    }

    try {
      Email::Sender::Simple->send( $mail, { transport => $self->{transport} } );
      last;
    }
    catch ($e) {
      die $e unless $e isa Email::Sender::Failure;
      die 'Permanent failure when sending email: ' . $e->message
        unless $e->isa('Email::Sender::Failure::Temporary');

      warn 'Temporary failure when sending email: ' . $e->message;
    }
  }
}

'He signs his name to a letter he just wrote';
