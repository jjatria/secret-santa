package Acme::SecretSanta::Renderer::String;

our $VERSION = '2022';

use Moo;
use Carp;
use String::Format 'stringf';

with 'Acme::SecretSanta::Role::Renderer';

use namespace::clean;

has format => ( is => 'rw', default => "[ %n ] --> [ %N ]\n" );

sub render_summary {
  my ($self) = @_;

  my $out = '';
  $out .= $self->render($_) for $self->santa->participants;

  $out;
}

sub render {
  my ($self, $person, @rest) = @_;

  my %source = (
    n => $person->name,
    a => $person->address,
    N => $person->target->name,
    A => $person->target->address,
  );

  sprintf stringf( $self->format, %source ), @rest;
}

q{It's beginning to look a lot like Christmas};
