package Acme::SecretSanta::Renderer::Email;

our $VERSION = '2022';

use Moo;
use Carp;
use Email::Stuffer;
use File::Share 'dist_dir';
use Path::Tiny 'path';
use Template;
use Encode 'decode';

with 'Acme::SecretSanta::Role::Renderer';

use namespace::clean;

has [qw( from password subject username )] => ( is => 'ro' );

has template         => ( is => 'ro', default => 'default' );
has summary_template => ( is => 'ro', default => 'summary' );

my $make_email = sub {
  my ( $self, %args ) = @_;

  $self->templator->process(
    $args{template},
    $args{params},
    \my $body,
    { binmode => ':utf8' }
  ) or croak $self->templator->error;

  $body =~ s/^\s*|\s+$//;

  return Email::Stuffer->new({
    to        => $args{to}   // '',
    from      => $self->from // '',
    text_body => $body,
    subject   => decode( 'UTF-8', $self->subject // '' ),
  })
};

has templator => (
  is => 'ro',
  lazy => 1,
  default => sub {
    my $path = path dist_dir 'Acme-SecretSanta';
    Template->new(
        INCLUDE_PATH => $path->child('templates')->canonpath,
        ENCODING     => 'utf8',
    );
  },
);

sub render_summary {
  my ( $self, $address, $extra ) = @_;

  croak 'Cannot render summary as email without a TO address'
    unless $address;

  $self->$make_email(
    params   => { santa => $self->santa, %{ $extra // {} } },
    template => $self->summary_template . '.tt',
    to       => $address,
  );
}

sub render {
  my ( $self, $person, %extra ) = @_;

  my $address = $person->address
    or croak 'Participant has no set email address';

  $self->$make_email(
    params   => { person => $person, %extra },
    template => $self->template . '.' . $person->language . '.tt',
    to       => $address,
  );
}

q{'Twas nigh afore Christmas at the Freemason's Hall};
