# ABSTRACT: A config loader for Acme::SecretSanta

package Acme::SecretSanta::Config;

use strict;
use warnings;
use Path::Tiny 'path';

our $VERSION = '2022';

use Carp 'croak';

use namespace::clean;

sub load {
    shift if $_[0] eq __PACKAGE__;
    my $path = path( shift );

    my ($ext) = $path =~ /^[^.].*\.([^.]+)$/;
    $ext = lc $ext;

    my %args = @_;
    %args = ( binmode => ':raw:encoding(UTF-8)' ) unless %args;

    my $data = $path->slurp(\%args);

    if ( $ext eq 'toml' ) {
        require TOML::Tiny;
        return TOML::Tiny::from_toml $data;
    }
    elsif ( $ext eq 'json' ) {
        require JSON::PP;
        return JSON::PP::decode_json $data;
    }

    croak sprintf 'No decoder supported for %s files', uc $ext;
}

1;
