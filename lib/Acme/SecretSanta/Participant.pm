package Acme::SecretSanta::Participant;

our $VERSION = '2022';

use Moo;

with 'Acme::SecretSanta::Role::Participant';

'Have yourself a Merry Little Christmas';
