package Acme::SecretSanta::Group;

our $VERSION = '2022';

use Moo;

with 'Acme::SecretSanta::Role::Base';

use Carp;
use Ref::Util 'is_blessed_ref';
use Scalar::Util 'weaken';
use Types::Standard 'Str';

use namespace::clean;

has name => ( is => 'ro', isa => Str, required => 1 );

sub add_member {
  my $self = shift;

  for (@_) {
    croak 'Cannot add member: not a reference'
      unless is_blessed_ref $_;

    weaken( $self->{members}{ $_->name } = $_ );

    $_->join_group($self);
  }

  $self;
}

sub remove_member {
  my $self = shift;
  delete @{ $self->{members} //= {} }{ map $_->name, @_ };
  $self;
}

sub total_members { scalar shift->members }

sub members { values %{ shift->{members} //= {} } }

sub is_member {
  my ( $self, $participant ) = @_;
  defined $self->{members}{ $participant->name };
}

'Let it snow, let it snow, let it snow';
