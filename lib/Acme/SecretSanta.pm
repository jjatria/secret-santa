# ABSTRACT: A Secret-Santa sorter which allows for arbitrary exceptions

package Acme::SecretSanta;

our $VERSION = '2022';

use Moo;
use Carp;
use Class::Load 'load_class';
use List::Util 'any';
use Ref::Util qw(
  is_blessed_ref
  is_plain_arrayref
  is_plain_hashref
  is_plain_ref
);

use Acme::SecretSanta::Config;
use Acme::SecretSanta::Participant;
use Acme::SecretSanta::Group;
use Acme::SecretSanta::Types qw(
  SantaRenderer
  SantaSorter
  SantaDispatcher
);

use namespace::clean;

has is_sorted => ( is => 'rw', lazy => 1, default => 0 );

has sorter => (
  is      => 'rw',
  lazy    => 1,
  isa     => SantaSorter,
  default => sub {
    require Acme::SecretSanta::Sorter::Shuffle;
    Acme::SecretSanta::Sorter::Shuffle->new( santa => $_[0] );
  },
);

has renderer => (
  is      => 'rw',
  lazy    => 1,
  isa     => SantaRenderer,
  default => sub {
    require Acme::SecretSanta::Renderer::String;
    Acme::SecretSanta::Renderer::String->new( santa => $_[0] );
  },
);

has dispatcher => (
  is      => 'rw',
  lazy    => 1,
  isa     => SantaDispatcher,
  default => sub {
    require Acme::SecretSanta::Dispatcher::Handle;
    Acme::SecretSanta::Dispatcher::Handle->new( santa => $_[0] );
  },
);

for my $method (qw( sorter renderer dispatcher )) {
  around $method => sub {
    my $orig = shift;
    my $self = shift;
    my ($class, @rest) = @_;

    return $self->$orig(@_) unless $class;

    $class = $class =~ /^\+/
      ? $class =~ s/^\+//r
      : 'Acme::SecretSanta::' . ucfirst($method) . '::' . $class;

    load_class $class;
    return $self->$orig( $class->new( santa => $self, @rest ) );
  };
}

sub sort     { shift->sorter->sort(@_)         }
sub render   { shift->renderer->render(@_)     }
sub dispatch { shift->dispatcher->dispatch(@_) }

sub add_participant {
  my $self = shift;

  $self->sorter->reset;

  map {
    my $p = $_->can('name')
      ? $_
      : Acme::SecretSanta::Participant->new( santa => $self, name => $_ );

    $self->{participants}{ $p->name } = $p;
  } @_;
}

sub remove_participant {
  my $self = shift;
  my @names = map $_->can('name') ? $_->name : $_, @_;
  $self->sorter->reset;
  delete @{ $self->{participants} //= {} }{@names};
}

sub get_participant {
  my $self = shift;
  my @names = map $_->can('name') ? $_->name : $_, @_;
  @{ $self->{participants} //= {} }{@names};
}

sub total_participants { scalar keys %{ shift->{participants} //= {} } }

sub participants { values %{ shift->{participants} //= {} } }

sub set_group {
  my ( $self, $name, @members ) = @_;

  croak 'Cannot set group: need a list of references'
    if any { !is_blessed_ref $_ } @members;

  $self->{groups}{$name}
    = Acme::SecretSanta::Group->new( name => $name )->add_member( @members );
}

sub remove_group {
  my ( $self, @groups ) = @_;

  croak 'Cannot remove group: need a list of references'
    if any { !is_blessed_ref $_ } @groups;

  delete $self->{groups}{ map $_->name, @groups };

  $self;
}

sub get_group { @{ shift->{groups} //= {} }{@_} }

sub total_groups { scalar keys %{ shift->{groups} //= {} } }

sub groups { values %{ shift->{groups} //= {} } }

sub load {
  croak 'Not enough arguments to load' unless scalar @_ >= 2;

  my ($class, $data, $extra) = @_;
  $extra //= {};

  $data = Acme::SecretSanta::Config::load($data) unless is_plain_ref $data;

  if ( is_plain_hashref $data ) {
    my @data;
    for ( keys %{ $data } ) {
      push @data, is_plain_hashref $data->{$_}
        ? { name => $_, %{ $data->{$_} } }
        : $data;
    }
    $data = \@data;
  }

  $data = [ $data ] unless is_plain_arrayref $data;

  # Create a new santa object
  my $self = $class->new( $extra );

  # Add new participants
  $self->add_participant(
    map Acme::SecretSanta::Participant->new(
      santa => $self,
      name  => $_->{name},
      $_->{address}  ? ( address  => $_->{address} )  : (),
      $_->{language} ? ( language => $_->{language} ) : (),
    ), @{$data}
  );

  # Add exceptions
  for my $d (@{$data}) {
    my $participant = $self->get_participant( $d->{name} );

    $participant->add_exceptions(
      map {
        $self->get_participant($_)
          or croak qq["$d->{name}" has "$_" as an exception, but no such participant exists];
      } @{ $d->{exceptions} }
    );
  }

  $self;
}

sub BUILD {
  my ($self, $arg) = @_;

  for my $method (qw( renderer sorter dispatcher )) {
    $self->$method->santa( $self ) unless defined $self->$method->santa;
  }
}

'For we need a little Christmas, right this very minute';
