requires 'Class::Load'          => 0;
requires 'Email::Sender'        => 0;
requires 'Email::Stuffer'       => 0;
requires 'Feature::Compat::Try' => 0;
requires 'File::Share'          => 0;
requires 'Object::Pad'          => 0;
requires 'Path::Tiny'           => 0;
requires 'Ref::Util'            => 0;
requires 'Role::EventEmitter'   => 0;
requires 'String::Format'       => 0;
requires 'Template'             => 0;
requires 'Time::HiRes'          => 0;
requires 'Type::Tiny'           => 0;
requires 'namespace::clean'     => 0;

feature 'SMTP' => sub {
  requires 'Authen::SASL';
  requires 'MIME::Base64';
};

on test => sub {
  requires 'Test2::Suite'  => 0;
  requires 'Capture::Tiny' => 0;
  requires 'Path::Tiny'    => 0;
  requires 'File::Share'   => 0;
  requires 'JSON::MaybeXS' => 0;
};
